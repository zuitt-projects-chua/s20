//alert("Hi")

/*
JSON Objects
Javascript Object Notation
-can be used in othe programming languages
used for serializing differetn data types
(process of converting data types intoseries of bytes for easier transmission of information)
bytes are information that a computer process to perform tasks
uses double qoutes for properties name
Syntax
{
	"property": "value",
	"property": "value",
}
*/
//Json Object
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}

//Json Array
"cities" : [
	{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines",
	},
	{
		"city": "Cebu City",
		"province" : "Cebu",
		"country" : "Philippines",
	}
];*/

/*
Json Methods  
*/

let batchesArr  = [
	{batchName: "Batch 169"},
	{batchName: "Batch 170"}
];

//JSON.stringify(<name>))
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: "Luke",
	age: 45,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);

let firstName = prompt("First Name:")
let lastName = prompt("Last Name:")
let age = prompt("Age:")
let address = {
	city: prompt("City:"),
	country: prompt("Country:")
}

let data2 = JSON.stringify({
	//the property firstname belongs to data2, value firstname refers to let variables
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(data2);

//Database stores JSON objects

//Converting stringified JSON to Javascript objects

let batchesJSON = `[
	{
		"batchName" :  169
	},
	{
		"batchName" : "Batch 170"
	}]`

console.log(JSON.parse(batchesJSON))




